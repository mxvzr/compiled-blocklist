#!/bin/bash
set -euf -o pipefail
echo "# generated on $(date --utc --rfc-3339=seconds)"
find blocklist.d -name "*.txt" -exec cat {} + \
    | grep --invert-match --extended-regexp "^$|^#" \
    | xargs curl --fail --location --max-time 10 --silent \
    | sed -e "s/#.*$//;s/^[\t ]*//;s/[\t ]*$//" \
    | grep --text --invert-match --extended-regexp \
        "^$|^#|^[\t\s]*$|localhost$|broadcasthost$|ip6-[a-z]*$" \
    | sed \
        -e "s/127\.0\.0\.1[\t ]*//;s/0\.0\.0\.0[\t ]*//;s/0 //;s/::[\t ]*//" \
        -e "s/;[0-9]*$//" \
    | grep --text --invert-match --extended-regexp "^$" \
    | sort | uniq | awk '{print "0.0.0.0\t" $0}'
